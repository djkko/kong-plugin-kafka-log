### 重要
此项目基于定时器及缓存kafka生产者实例，许久不维护，github上已有人使用cosocket子请求无阻塞重写。项目地址：
https://github.com/Optum/kong-kafka-log

### 由于原项目地址代码因kong版本问题，出现bug，此为修复版本。
### 项目原地址：
- https://github.com/yskopets/kong-plugin-kafka-log
### 修复：
原项目因为Kong版本升级不可用，原因是代码中使用uuid作为key缓存了kafka实例，
uuid是动态设置的，利用了参数校验可以拿到schema属性，
但是根据官方文档，在新的参数校验方法中没有看到支持获取schema参数，
也就无法动态生成uuid。这里把原uuid属性改成cacheKey，
需要每次更新插件信息时同时更新cacheKey属性值，否则修改不生效。
### 注意
需要每次更新插件信息时同时更新cacheKey属性值，否则修改不生效


  


